#!/bin/bash

#build in jenkins by12

REG_URL=ccr.ccs.tencentyun.com/jayqqaa12/test  #这里我用tx云的容器仓库管理 可以换成你自己的 不然登录不了



H=`date +%H`
H=`expr 8 + $H` #时区不太好改直接加8吧 

TAG=$REG_URL:`date +%y%m%d-`$H-`date +%M`
 

docker run --rm  -v /mnt/maven:/root/.m2   \
 -v /mnt/jenkins_home/workspace/$JOB_NAME:/usr/src/mvn -w /usr/src/mvn/ \
 maven:3.3.3-jdk-8 mvn clean install -Dmaven.test.skip=true
 

docker build -t  $TAG  $WORKSPACE/.

docker tag $JOB_NAME $REG_URL


docker login --username=xxx --password=xxxx  ccr.ccs.tencentyun.com   # 这里我用tx云的容器仓库管理 第一次需要密码 先手动登录一次

docker push   $TAG



# 下面是单机模式

function local(){
if docker   ps -a| grep -i $JOB_NAME; then
  docker rm -f  $JOB_NAME
fi

docker run  -d  -p 8080:8080  --name $JOB_NAME  $TAG 
}



option="${1}"


case ${option} in
   
    -DEV)
        local
      ;;

    -TEST)
        local    
     ;;
    -PROD)
        export REPLICAS=2 
	export IMAGE=$TAG
        # 下面是swarm 模式 直接读取 docker-compase.yaml 配置来启动
        docker stack deploy -c docker-compose.yml  $JOB_NAME
     ;;
 
esac